package hse.construction.GateWay.config;

import hse.construction.GateWay.filters.AuthFilter;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;


@Configuration
public class GateWayConfiguration {

    @Bean
    public RouteLocator routes(
            RouteLocatorBuilder builder,
             AuthFilter authFilter) {
        return builder.routes()
                .route("auth-service-route", r -> r.path("/authentication-service/**")
                        .filters(f ->
                                f.rewritePath("/authentication-service(?<segment>/?.*)", "$\\{segment}")
                                        .filter(authFilter.apply(
                                                new AuthFilter.Config())))
                        .uri("lb://authentication-service"))
                .route("ticket-service-route", r -> r.path("/ticket-service/**")
                        .filters(f ->
                                f.rewritePath("/ticket-service(?<segment>/?.*)", "$\\{segment}")
                                        .filter((authFilter.apply(
                                                new AuthFilter.Config()))))
                        .uri("lb://ticket-service"))
                .build();

    }
    @Bean
    @LoadBalanced
    public WebClient.Builder loadBalancedWebClientBuilder() {
        return WebClient.builder();
    }

}


