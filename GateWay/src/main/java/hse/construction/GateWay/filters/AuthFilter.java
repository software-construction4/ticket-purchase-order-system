
package hse.construction.GateWay.filters;

import hse.construction.GateWay.model.TokenValidationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;


import java.util.List;
import java.util.function.Predicate;

@Component
@Slf4j

public class AuthFilter extends AbstractGatewayFilterFactory<AuthFilter.Config> {
    final List<String> apiRegisterEndpoints = List.of("/register", "/login");

    private final WebClient.Builder webClientBuilder;

    public AuthFilter(WebClient.Builder webClientBuilder) {
        super(Config.class);
        this.webClientBuilder = webClientBuilder;
    }

    Predicate<ServerHttpRequest> isNotRegisterApi = r -> apiRegisterEndpoints.stream()
            .noneMatch(uri -> r.getURI().getPath().contains(uri));

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();
            log.info("**************************************************************************");
            log.info("URL is - " + request.getURI().getPath());
            log.info(String.valueOf(isNotRegisterApi.test(request)));
            if (isNotRegisterApi.test(request)) {
                log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                if (!request.getHeaders().containsKey("Authorization")) {
                    ServerHttpResponse response = exchange.getResponse();
                    response.setStatusCode(HttpStatus.UNAUTHORIZED);
                    return response.setComplete();
                }
                String bearerToken = request.getHeaders().getFirst("Authorization");
                log.info("Bearer Token: " + bearerToken);
                return webClientBuilder.build().get()
                        .uri("lb://authentication-service/validateToken")
                        .header("Authorization", bearerToken)
                        .retrieve().bodyToMono(TokenValidationResponse.class)
                        .map(response -> {
                            exchange.getRequest().mutate().header("email", response.getEmail());
                            exchange.getRequest().mutate().header("authority", response.getAuthority());
                            log.info("Successfully validated");
                            return exchange;
                        }).flatMap(chain::filter).onErrorResume(error -> {
                            log.info("Error occurred");
                            ServerHttpResponse response = exchange.getResponse();
                            response.setStatusCode(HttpStatus.BAD_REQUEST);
                            return response.setComplete();
                        });
            }
            return chain.filter(exchange);
        };
    }

    public static class Config {
    }
}