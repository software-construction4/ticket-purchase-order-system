package hse.construction.ticket_service.converters;

import hse.construction.ticket_service.models.Order;
import hse.construction.ticket_service.responsies.OrderResponse;
import hse.construction.ticket_service.services.StationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
@RequiredArgsConstructor
public class OrderConverter {
    private final StationService stationService;

    private String convertStatus(Integer status) {
        switch (status) {
            case 1:
                return "check";
            case 2:
                return "success";
            case 3:
                return "rejection";
            default:
                throw new IllegalStateException("Unexpected value:");
        }
    }
    private OrderResponse convert(Order order){
        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setFromStation(stationService.getStationName(order.getFromStationId()));
        orderResponse.setToStation(stationService.getStationName(order.getToStationId()));
        orderResponse.setStatus(convertStatus(order.getStatus()));
        orderResponse.setCreated(order.getCreated().toLocalDateTime());
        return orderResponse;
    }
    public List<OrderResponse> convertToResponse(List<Order> orders){
        return orders.stream().map(this::convert).toList();
    }
}
