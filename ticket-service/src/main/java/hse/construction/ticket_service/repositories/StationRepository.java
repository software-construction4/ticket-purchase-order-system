package hse.construction.ticket_service.repositories;


import hse.construction.ticket_service.models.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface StationRepository extends JpaRepository<Station, Long> {
     @Query("select st.id from Station st where st.station = ?1")
     Long findByStation(String station);

     @Query("select st.station from Station st where st.id = ?1")
     String findStationById(Long id );

}
