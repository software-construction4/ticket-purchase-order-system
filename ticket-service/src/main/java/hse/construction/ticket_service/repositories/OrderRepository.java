package hse.construction.ticket_service.repositories;

import hse.construction.ticket_service.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByStatus(Integer status);

    List<Order> findAllByUserId(Long userId);
}
