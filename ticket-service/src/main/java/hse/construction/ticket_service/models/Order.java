package hse.construction.ticket_service.models;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "orders")
@Data
@Slf4j
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "user_id")
    private Long userId;

    @NotNull
    @Column(name = "from_station_id ")
    private Long fromStationId;

    @NotNull
    @Column(name = "to_station_id ")
    private Long toStationId;

    @NotNull
    @Column(name = "status")
    private Integer status;

    @Column(name = "created")
    private Timestamp created;

    public Order() {

    }


    @PrePersist
    private void init() {
        created = Timestamp.valueOf(LocalDateTime.now());
        status = 1;
    }

    public Order(Long fromStationId, Long toStationId, Long userId){
        this.fromStationId = fromStationId;
        this.toStationId = toStationId;
        this.userId = userId;
    }


}

