package hse.construction.ticket_service.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class OrderDTO {

    @NotNull
    private String fromStation;

    @NotNull
    private String toStation;

}