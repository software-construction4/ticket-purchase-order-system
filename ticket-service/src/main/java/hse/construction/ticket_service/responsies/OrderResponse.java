package hse.construction.ticket_service.responsies;


import hse.construction.ticket_service.models.Order;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class OrderResponse {

    private String fromStation;

    private String toStation;

    private String status;

    private LocalDateTime created;

}
