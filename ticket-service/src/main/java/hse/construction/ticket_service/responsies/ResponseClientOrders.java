package hse.construction.ticket_service.responsies;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ResponseClientOrders {
    List<OrderResponse> orders;
    String email;

}
