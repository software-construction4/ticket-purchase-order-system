package hse.construction.ticket_service.responsies;

import hse.construction.ticket_service.models.Station;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ResponseStations {
    List<Station> stations;
}
