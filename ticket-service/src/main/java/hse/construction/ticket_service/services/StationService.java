package hse.construction.ticket_service.services;

import hse.construction.ticket_service.models.Station;
import hse.construction.ticket_service.repositories.StationRepository;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
public class StationService {
    private final StationRepository stationRepository;

    public List<Station> getAllStations(){
        return stationRepository.findAll();
    }

    public Long getStationIdByName(String stationName){
        return stationRepository.findByStation(stationName);
    }

    public String getStationName(Long stationId){
        return stationRepository.findStationById(stationId);
    }
}
