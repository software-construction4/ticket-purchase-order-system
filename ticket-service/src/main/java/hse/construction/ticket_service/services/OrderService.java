package hse.construction.ticket_service.services;

import hse.construction.ticket_service.dto.OrderDTO;
import hse.construction.ticket_service.models.Order;
import hse.construction.ticket_service.repositories.OrderRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
@AllArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final StationService stationService;

    public List<Order> getOrdersWithStatus(Integer status) {
        return orderRepository.findAllByStatus(status);
    }

    public boolean createOrder(OrderDTO orderDTO, Long userId) {
        Long stationFrom = stationService.getStationIdByName(orderDTO.getFromStation());
        Long stationTo = stationService.getStationIdByName(orderDTO.getToStation());
        if(stationTo == null || stationFrom ==null){
            return false;
        }
        Order order = new Order(stationFrom,stationTo,userId);
        orderRepository.save(order);
        return true;
    }

    public List<Order> getOrdersByUserId(Long userId) {
        return orderRepository.findAllByUserId(userId);
    }
}
