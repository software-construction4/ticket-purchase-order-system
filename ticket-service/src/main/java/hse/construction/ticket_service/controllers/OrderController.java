package hse.construction.ticket_service.controllers;


import hse.construction.ticket_service.converters.OrderConverter;
import hse.construction.ticket_service.dto.OrderDTO;
import hse.construction.ticket_service.models.Order;
import hse.construction.ticket_service.models.User;
import hse.construction.ticket_service.responsies.OrderResponse;
import hse.construction.ticket_service.responsies.ResponseClientOrders;
import hse.construction.ticket_service.services.OrderService;
import hse.construction.ticket_service.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;
    private final UserService userService;
    private final OrderConverter orderConverter;

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createOrder(@RequestBody @Valid OrderDTO orderDTO) {
        User user = userService.getUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        if (!orderService.createOrder(orderDTO, user.getId())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid stations");
        }
        return ResponseEntity.ok("Order created successfully");
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseClientOrders> clientOrders(){
        User user = userService.getUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        List<Order> orders = orderService.getOrdersByUserId(user.getId());
        List<OrderResponse> orderResponses = orderConverter.convertToResponse(orders);
        ResponseClientOrders responseClientOrders = new ResponseClientOrders(orderResponses, SecurityContextHolder.getContext().getAuthentication().getName());
        return ResponseEntity.status(HttpStatus.OK).body(responseClientOrders);
    }
}