package hse.construction.ticket_service.controllers;

import hse.construction.ticket_service.responsies.ResponseStations;
import hse.construction.ticket_service.services.StationService;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Data
public class StationController {
    private final StationService stationService;

    @GetMapping(value = "/stations", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseStations> getAllStations() {
        ResponseStations responseStationsModel = new ResponseStations(stationService.getAllStations());
        return new ResponseEntity<>(responseStationsModel, HttpStatus.OK);
    }
}
