package hse.construction.ticket_service.schedules;

import hse.construction.ticket_service.models.Order;
import hse.construction.ticket_service.services.OrderService;
import jakarta.transaction.Transactional;
import lombok.Data;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Data
@Service
public class UpdateOrderStatus {
    private final OrderService orderService;
    private final Random random = new Random();
    @Scheduled(fixedDelay = 30)
    @Transactional
    public void updateOrderStatus(){
        List<Order> ordersWithCheckStatus = orderService.getOrdersWithStatus(1);
        for(Order order : ordersWithCheckStatus){
            order.setStatus(random.nextInt(2)+2);
        }
    }
}
