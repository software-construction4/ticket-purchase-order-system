create table station (
     id serial primary key,
     station varchar(50) not null
);

CREATE TABLE orders (
       id serial primary key,
       user_id int not null,
       from_station_id int not null,
       to_station_id int not null,
       status int not null,
       created timestamp default CURRENT_TIMESTAMP,
       FOREIGN KEY (user_id) REFERENCES users(id),
       FOREIGN KEY (from_station_id) REFERENCES station(id),
       FOREIGN KEY (to_station_id) REFERENCES station(id)
);

INSERT INTO station (station) VALUES
  ('Moscow'),
  ('Saint Petersburg'),
  ('Novosibirsk'),
  ('Yekaterinburg'),
  ('Kazan'),
  ('Nizhny Novgorod'),
  ('Chelyabinsk'),
  ('Samara'),
  ('Omsk')
