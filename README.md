# Ticket purchase order system



## Запуск 

### Для каждого микросервиса создать образ:
```
docker build . -t ticket
docker build . -t eureka
docker build . -t gateway
docker build . -t security-service
```
### Запустить docker compose в микросервисе gateway:
```
docker compose up
```

## Описание 
#### auth-service - сервис авторизации
#### ticket-service - сервис заказа билетов
#### gateway - единая точка входа
#### eureka - discovery service

## Ссылка на postman коллекцию https://mkuvatov79mailru.postman.co/workspace/m.kuvatov79%40mail.ru-Workspace~202fd3c2-11dc-45fe-86b1-e3edc4d0054a/collection/34534492-37911131-7d67-4320-9653-867020b90f62?action=share&creator=34534492

