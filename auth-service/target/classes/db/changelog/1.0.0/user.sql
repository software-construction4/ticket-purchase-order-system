create table users (
   id serial primary key ,
   nickname varchar(50) not null,
   email varchar(100) unique not null ,
   password varchar(255) not null ,
   created timestamp default current_timestamp,
   active boolean not null,
   role   varchar(10)
       constraint ch_role check (role in ('0', '1'))
);

