package hse.constraction.securityopenapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityOpenApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityOpenApiApplication.class, args);
	}

}
