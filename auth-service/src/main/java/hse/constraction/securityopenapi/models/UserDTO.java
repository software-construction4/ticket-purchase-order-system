package hse.constraction.securityopenapi.models;

import hse.constraction.securityopenapi.validators.ValidRole;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UserDTO {
    @NotBlank
    public String nickname;

    @NotBlank
    public String password;

    @NotBlank
    @Email
    public String email;

    @ValidRole
    public String role;

}
