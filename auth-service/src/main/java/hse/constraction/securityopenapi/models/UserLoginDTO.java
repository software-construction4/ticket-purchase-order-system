package hse.constraction.securityopenapi.models;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UserLoginDTO {
    @Email
    @NotBlank
    public String email;

    @NotBlank
    public String password;
}
