package hse.constraction.securityopenapi.models;

import hse.constraction.securityopenapi.enums.Role;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import jakarta.validation.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.List;


@Entity
@Table(name = "users")
@Data
@Slf4j
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "nickname")
    private String nickname;


    @Column(name = "active")
    private boolean active;


    @NotBlank
    @Column(name = "password", length = 1000)
    private String password;

    @Column(name = "role")
    private Role role;

    @NotBlank
    @Email
    private String email;

    @Column(name = "created")
    private Timestamp created;

    public User() {

    }

    public boolean hasRole(Role role){
        return role == this.role;
    }

    public User(UserDTO userDTO){
        this.role = Role.valueOf(userDTO.role);
        this.nickname = userDTO.nickname;
        this.password = userDTO.password;
        this.email = userDTO.email;
    }

    @PrePersist
    private void init() {
        created = Timestamp.valueOf(LocalDateTime.now());}

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return List.of(role);
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }
}