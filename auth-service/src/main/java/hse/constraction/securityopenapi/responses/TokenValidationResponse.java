package hse.constraction.securityopenapi.responses;



import hse.constraction.securityopenapi.enums.Role;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Builder
@ToString
@Data
public class TokenValidationResponse {
    private String status;
    private boolean isAuthenticated;
    private String methodType;
    private String email;
    private Role authority;
}
