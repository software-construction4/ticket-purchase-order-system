package hse.constraction.securityopenapi.validators;


import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class RoleValidator implements ConstraintValidator<ValidRole, String> {

    private static final List<String> VALID_ROLES = Arrays.asList("ROLE_USER", "ROLE_ADMIN");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || VALID_ROLES.contains(value);
    }
}