package hse.constraction.securityopenapi.services;




import hse.constraction.securityopenapi.models.User;
import hse.constraction.securityopenapi.models.UserDTO;
import hse.constraction.securityopenapi.models.UserLoginDTO;
import hse.constraction.securityopenapi.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
@Service
@RequiredArgsConstructor
public class UserService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final CustomUserDetailService customUserDetailService;
    private final JwtService jwtService;
    public boolean isUserUnique(User user){
        return userRepository.findByEmail(user.getEmail()) == null;
    }

    public ResponseEntity<String> createNewUser(UserDTO user) throws RuntimeException {
        User newUser = new User(user);
        if (!isUserUnique(newUser)){
            throw new RuntimeException("User already exists");
        }
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        newUser.setPassword(encodedPassword);
        userRepository.save(newUser);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body("User registered successfully");
    }

    public ResponseEntity<String> login(UserLoginDTO userLoginDTO){
        UserDetails userDetails;
        try{
             userDetails = customUserDetailService.loadUserByUsername(userLoginDTO.email);
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User not found");
        }
        String jwt = jwtService.generateToken(userDetails);
        return ResponseEntity.ok(jwt);
    }
}
