package hse.constraction.securityopenapi.services;


import io.jsonwebtoken.security.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Jwts;

import java.util.HashMap;
import java.util.Map;
import java.util.Date;
import java.util.function.Function;
import io.jsonwebtoken.Claims;
import javax.crypto.SecretKey;

@Service
@Slf4j
public class JwtService {
    SecretKey SECRETE_KEY;
    public JwtService() {

        SECRETE_KEY = Jwts.SIG.HS256.key().build();
        log.info("Secrete Key:" + SECRETE_KEY.toString());
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) throws SignatureException {

        return Jwts.parser()
                .verifyWith(SECRETE_KEY) // Устанавливаем ключ для подписи
                .build()
                .parseSignedClaims(token)
                .getPayload(); // Парсим и валидируем токен

    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails.getUsername());
    }

    private String createToken(Map<String, Object> claims, String subject) {
        try {
            return Jwts.builder().subject(subject).claims(claims)
                    .issuedAt(new Date(System.currentTimeMillis()))
                    .expiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10)) // 10 hours
                    .issuer("spring-security")
                    .signWith(SECRETE_KEY)
                    .compact();
        } catch (SignatureException exception){
            throw new SignatureException("Invalid signature");
        }
    }

    public boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
}
