
package hse.constraction.securityopenapi.filter;

import hse.constraction.securityopenapi.services.JwtService;
import io.jsonwebtoken.security.SignatureException;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@AllArgsConstructor
@Slf4j
public class JwtRequestFilter extends OncePerRequestFilter {
    private final UserDetailsService userDetailsService;
    private final JwtService jwtService;

    @Override
    protected void doFilterInternal( HttpServletRequest httpServletRequest,  HttpServletResponse httpServletResponse,FilterChain filterChain) throws ServletException, IOException, SignatureException {
        log.info("**************************************************************************");
        log.info("URL is - " + httpServletRequest.getRequestURI());
        String bearerToken = httpServletRequest.getHeader("Authorization");
        log.info("Bearer Token:"+bearerToken );
        String email = null;
        String jwtToken = null;
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            jwtToken = bearerToken.substring(7);
            log.info("jwtToken Token:"+jwtToken );
            try {
                email = jwtService.extractUsername(jwtToken);
            } catch (SignatureException e){
                sendErrorResponse(httpServletResponse);
                return;
            }

        } else {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
        if (email != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = this.userDetailsService.loadUserByUsername(email);
            if (jwtService.validateToken(jwtToken, userDetails)) {
                Authentication usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities()
                );

                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                httpServletRequest.setAttribute("email", email);
                httpServletRequest.setAttribute("authority", userDetails.getAuthorities().iterator().next());
            } else {
                sendErrorResponse(httpServletResponse);
                return;
            }
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
    private void sendErrorResponse(HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");
        response.getWriter().write(String.format("{\"error\": \"%s\"}", "Invalid JWT token"));
    }
}
