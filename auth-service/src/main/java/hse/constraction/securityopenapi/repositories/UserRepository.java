package hse.constraction.securityopenapi.repositories;

import hse.constraction.securityopenapi.models.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

}
