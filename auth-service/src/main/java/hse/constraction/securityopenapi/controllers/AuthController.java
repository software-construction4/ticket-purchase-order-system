package hse.constraction.securityopenapi.controllers;


import hse.constraction.securityopenapi.enums.Role;
import hse.constraction.securityopenapi.responses.TokenValidationResponse;
import hse.constraction.securityopenapi.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import hse.constraction.securityopenapi.models.UserDTO;
import hse.constraction.securityopenapi.models.UserLoginDTO;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AuthController {
    private final UserService userService;


    @PostMapping("/register")
    public ResponseEntity<String> createUser(@Valid @RequestBody UserDTO user) {
        log.info("/register");
        return userService.createNewUser(user);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@Valid @RequestBody UserLoginDTO userLoginDTO){
        log.info("/login");
        return userService.login(userLoginDTO);
    }

    @GetMapping(value = "/validateToken", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<TokenValidationResponse> validateToken(HttpServletRequest request){
        log.info("/validateToken");
        log.info(request.getParameter("authority"));
        String email = (String) request.getAttribute("email");
        Role authority = (Role) request.getAttribute("authority");
        return ResponseEntity.ok(TokenValidationResponse.builder().status("OK").methodType(HttpMethod.GET.name())
                .email(email).authority(authority)
                .isAuthenticated(true).build());
    }

}
